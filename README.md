# Construirea unei aplicații Android cu criptare End to End folosind metoda AES (Advanced Encryption Standard) în baza de date Firebase de la zero.

## Studenți
1. Gîrz Sebastian - 1641A
2. Muresan Sebastian - 1641B
3. Roibu Remus - 1641B
4. Socaciu Călin - 1641B
## Prof. Coordonator
1. Mang Ioan
## Cerințe
1. Diverse setări ale fișierelor gradle.
2. Pentru criptare și decriptare se utilizeaza instanța AES a Android Cipher.
3. Șirul de mesaje este mai întâi convertit în octeți, apoi criptat folosind metoda AES și apoi octetul este convertit înapoi în șir utilizând un set de caractere standard.
4. Acest șir de octeți criptat este apoi stocat în Firebase.
5. Deci, deoarece datele stocate în baza de date Firebase sunt în formă criptată, chiar dacă cineva poate obține accesul la baza de date, mesajul real nu va putea fi citit de către persoana respectivă până când aceasta nu va avea metoda de criptare și cheia disponibile cu l.
6. Pe partea de decriptare, șirul primit din baza de date este convertit înapoi în octet folosind același set de caractere.
7. Odată ce datele de octeți criptate sunt disponibile, cifrarea modului de decriptare este utilizată pentru a decripta datele.
8. Datele decriptate sunt apoi convertite înapoi din octet în format lizibil String.
9. Apoi acel șir de ieșire, după ce este sortat în ordine cronologică, este afișat în lista de vizualizare a aspectului aplicației noastre pentru ca utilizatorul să o poata vedea.

# Proiect
Prin această aplicație am dorit să ne axăm mai mult pe partea de criptare și de decriptare a mesajelor. Prima și prima oară a trebuit să ne hotărâm ce Compile Sdk version vom folosi. Pentru aceasta am ales API 30: Android 11.0, din cauză că este cea mai nouă versiune de Android, a fost eliberat pe data de 8 Septembrie 2020, iar majoritatea telefoanelor ce vor apărea în 2021 din marca Samsung Galaxy vor avea versiunea de Android 11.
    
Pentru a putea face conexiunea cu Firebase/Database a fost nevoie să aflăm SHA key, aceasta provenind dintr-o familie de funcții criptografice concepute pentru a păstra securitatea datelor. Pentru a putea accesa SHA key, aceasta fiind generată local la accesarea tabului Gradle, deschiderea proiectului, intrarea în folderul Tasks, android și pornirea fișierului singingReport. După crearea proiectului și sincronizarea acestuia cu aplicația noastră, prin introducerea dependențelor în fisierele Gradle Scripts, build.gradle Project și Module + gradle.properties, adăugând librăriile necesare.

build.gradle (ProjectName)

> *  `classpath 'com.google.gms:google-services:4.3.4'`

build.gradle (:app)

> *   `dependencies {`
> *   `implementation 'com.google.firebase:firebase-analytics'`
> *   `implementation 'androidx.core:core:1.5.0-alpha05'`
> *   `implementation 'com.google.firebase:firebase-core:17.0.0'`
> *   `implementation 'com.google.firebase:firebase-database:17.0.0'`
> *   `implementation platform('com.google.firebase:firebase-bom:26.1.1')`
> *   `implementation 'androidx.appcompat:appcompat:1.2.0'`
> *   `implementation 'com.google.android.material:material:1.2.1'`
> *   `implementation 'androidx.constraintlayout:constraintlayout:2.0.4'`
> *   `}`
> *   `apply plugin: 'com.android.application'`
> *   `apply plugin: 'com.google.gms.google-services'`
    
gradle.properties

> *   `android.useAndroidX=true`
> *   `android.enableJetifier=true`
> *   `android.enableJetfier = true`

Am continuat cu designul interfeței din fișierul activity_main.xml. Având în vedere că am dorit să ne axăm mai mult pe partea de implementare am creat un design simplu și rapid, care conține un ListView, EditText și un Button. În ListView vom afișa mesajele din interiorul bazei de date, împreună cu data la care a fost trimis mesajul. În EditText vom introduce mesajul pe care îl dorim să îl transmitem în baza de date și să îl afișăm în ListView, apăsând butonul Send.

![design](https://gitlab.com/facultate-1641b/AndroidAppAES/-/raw/master/images/activity_main.jpg)

Cod activity_main.xml:

> *  `<?xml version="1.0" encoding="utf-8"?>`
> *  `<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"`
> *  `    xmlns:app="http://schemas.android.com/apk/res-auto"`
> *  `    xmlns:tools="http://schemas.android.com/tools"`
> *  `    android:layout_width="match_parent"`
> *  `    android:layout_height="match_parent"`
> *  `    tools:context=".MainActivity">`
> *  `    <Button`
> *  `        android:id="@+id/button"`
> *  `        android:layout_width="wrap_content"`
> *  `        android:layout_height="wrap_content"`
> *  `        android:layout_marginStart="320dp"`
> *  `        android:layout_marginTop="680dp"`
> *  `        android:onClick="sendButton"`
> *  `        android:text="@string/send" />`
> *  `    <EditText`
> *  `        android:id="@+id/editText"`
> *  `        android:layout_width="320dp"`
> *  `        android:layout_height="wrap_content"`
> *  `        android:layout_marginTop="680dp"`
> *  `        android:autofillHints=""`
> *  `        android:ems="10"`
> *  `        android:gravity="start|top"`
> *  `        android:hint="@string/type_your_message_here"`
> *  `        android:inputType="textMultiLine" />`
> *  `    <ListView`
> *  `        android:id="@+id/listView"`
> *  `        android:layout_width="match_parent"`
> *  `        android:layout_height="650dp">`
> *  `    </ListView>`
> *  `</RelativeLayout>`

Pentru criptare și decriptare se utilizează instanța AES a Android Cipher. 

Pentru criptare șirul de mesaje este mai întâi convertit în octeți, apoi criptat folosind metoda AES și apoi octetul criptat este convertit înapoi în șir utilizând un set de caractere standard. Acest șir de octeți criptat este apoi stocat în Firebase.

> *  `private final byte[] encryptionKey = {9, 115, 51, 86, 105, 4, -31, -23, -68, 88, 17, 20, 3, -105, 119, -53};`
> *  `secretKeySpec = new SecretKeySpec(encryptionKey, "AES");`
> *  `private String AESEncryptionMethod(String string) {`
> *  `       byte[] stringByte = string.getBytes();`
> *  `       byte[] encryptedByte = new byte[stringByte.length];`
> *  `       try {`
> *  `           cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);`
> *  `           encryptedByte = cipher.doFinal(stringByte);`
> *  `       } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {`
> *  `           e.printStackTrace();`
> *  `       }`
> *  `       String returnString = null;`
> *  `       try {`
> *  `           returnString = new String(encryptedByte, "ISO-8859-1");`
> *  `       } catch (UnsupportedEncodingException e) {`
> *  `           e.printStackTrace();`
> *  `       }`
> *  `       return returnString;`
> *  `   }`

Deoarece datele stocate în baza de date Firebase sunt în formă criptată, chiar dacă cineva poate obține accesul la baza de date, mesajul real nu va putea fi citit de către persoana respectivă până când aceasta nu are metoda de criptare și secretKeySpec cu el.

![database](https://gitlab.com/facultate-1641b/AndroidAppAES/-/raw/master/images/database.JPG)

În mod similar, pe partea de decriptare, șirul primit din baza de date este convertit înapoi în octet folosind același set de caractere (utilizat pe partea de criptare). Odată ce datele de octeți criptate sunt disponibile, cifrarea modului de decriptare este utilizată pentru a decripta datele. Datele decriptate sunt ulterior convertite înapoi din octet în format lizibil String. Apoi, șirul de ieșire, după sortare în ordine cronologică, este afișat în lista de vizualizare a aspectului aplicației noastre, pentru ca utilizatorul să o poată vedea.

> * `private String AESDecryptionMethod(String string) throws UnsupportedEncodingException {`
> * `    byte[] EncryptedByte = string.getBytes("ISO-8859-1");`
> * `    String decryptedString = string;`
> * `    byte[] decryption;`
> * `    try {`
> * `        decipher.init(Cipher.DECRYPT_MODE, secretKeySpec);`
> * `        decryption = decipher.doFinal(EncryptedByte);`
> * `        decryptedString = new String(decryption);`
> * `    } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {`
> * `        e.printStackTrace();`
> * `    }`
> * `    return decryptedString;`
> * `}`



> * `package com.example.aestest;`
> * ``
> * `import androidx.annotation.NonNull;`
> * `import androidx.appcompat.app.AppCompatActivity;`
> * ``
> * `import android.annotation.SuppressLint;`
> * `import android.os.Bundle;`
> * `import android.view.View;`
> * `import android.widget.ArrayAdapter;`
> * `import android.widget.EditText;`
> * `import android.widget.ListView;`
> * ``
> * ``
> * `import com.google.firebase.database.DataSnapshot;`
> * `import com.google.firebase.database.DatabaseError;`
> * `import com.google.firebase.database.DatabaseReference;`
> * `import com.google.firebase.database.FirebaseDatabase;`
> * `import com.google.firebase.database.ValueEventListener;`
> * `import com.google.firebase.database.snapshot.StringNode;`
> * ``
> * `import java.io.Console;`
> * `import java.io.UnsupportedEncodingException;`
> * `import java.lang.reflect.Array;`
> * `import java.security.InvalidKeyException;`
> * `import java.security.NoSuchAlgorithmException;`
> * `import java.util.Arrays;`
> * `import java.util.Date;`
> * `import java.util.Objects;`
> * ``
> * `import javax.crypto.BadPaddingException;`
> * `import javax.crypto.Cipher;`
> * `import javax.crypto.IllegalBlockSizeException;`
> * `import javax.crypto.NoSuchPaddingException;`
> * `import javax.crypto.spec.SecretKeySpec;`
> * ``
> * `public class MainActivity extends AppCompatActivity {`
> * ``
> * ``
> * `    private EditText editText;`
> * `    private ListView listView;`
> * ``
> * `    private String stringMessage;`
> * `    private final byte[] encryptionKey = {9, 115, 51, 86, 105, 4, -31, -23, -68, 88, 17, 20, 3, -105, 119, -53};`
> * `    private DatabaseReference databaseReference;`
> * `    private Cipher cipher, decipher;`
> * `    private SecretKeySpec secretKeySpec;`
> * ``
> * ``
> * `    @SuppressLint("GetInstance")`
> * `    @Override`
> * `    protected void onCreate(Bundle savedInstanceState) {`
> * `        super.onCreate(savedInstanceState);`
> * `        setContentView(R.layout.activity_main);`
> * ``
> * `        editText = findViewById(R.id.editText);`
> * `        listView = findViewById(R.id.listView);`
> * ``
> * `        try {`
> * `            databaseReference = FirebaseDatabase.getInstance().getReference("Message");`
> * ``
> * `            try {`
> * `                cipher = Cipher.getInstance("AES");`
> * `                decipher = Cipher.getInstance("AES");`
> * `            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {`
> * `                e.printStackTrace();`
> * `            }`
> * ``
> * `            secretKeySpec = new SecretKeySpec(encryptionKey, "AES");`
> * ``
> * `            databaseReference.addValueEventListener(new ValueEventListener() {`
> * `                @Override`
> * `                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {`
> * `                    try {`
> * `                        stringMessage = dataSnapshot.getValue().toString();`
> * `                        stringMessage = stringMessage.substring(1, stringMessage.length() - 1);`
> * ``
> * `                        String[] stringMessageArray = stringMessage.split(", ");`
> * `                        Arrays.sort(stringMessageArray);`
> * `                        String[] stringFinal = new String[stringMessageArray.length * 2];`
> * ``
> * `                        for (int i = 0; i < stringMessageArray.length; i++) {`
> * `                            String[] stringKeyValue = stringMessageArray[i].split("=", 2);`
> * `                            stringFinal[2 * i] = (String) android.text.format.DateFormat.format("dd-MM-yyyy hh:mm:ss", Long.parseLong(stringKeyValue[0]));`
> * `                            stringFinal[2 * i + 1] = AESDecryptionMethod(stringKeyValue[1]);`
> * `                        }`
> * ``
> * ``
> * `                        listView.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, stringFinal));`
> * `                    } catch (Exception e) {`
> * `                        e.printStackTrace();`
> * `                    }`
> * `                }`
> * ``
> * `                @Override`
> * `                public void onCancelled(@NonNull DatabaseError databaseError) {`
> * ``
> * `                }`
> * `            });`
> * `        } catch (Exception e) {`
> * `            e.printStackTrace();`
> * `        }`
> * ``
> * `    }`
> * ``
> * `    public void sendButton(View view) {`
> * ``
> * `        Date date = new Date();`
> * `        databaseReference.child(Long.toString(date.getTime())).setValue(AESEncryptionMethod(editText.getText().toString()));`
> * `        editText.setText("");`
> * ``
> * `    }`
> * ``
> * `    private String AESEncryptionMethod(String string) {`
> * ``
> * `        byte[] stringByte = string.getBytes();`
> * `        byte[] encryptedByte = new byte[stringByte.length];`
> * ``
> * `        try {`
> * `            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);`
> * `            encryptedByte = cipher.doFinal(stringByte);`
> * `        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {`
> * `            e.printStackTrace();`
> * `        }`
> * ``
> * `        String returnString = null;`
> * ``
> * `        try {`
> * `            returnString = new String(encryptedByte, "ISO-8859-1");`
> * `        } catch (UnsupportedEncodingException e) {`
> * `            e.printStackTrace();`
> * `        }`
> * `        return returnString;`
> * `    }`
> * ``
> * `    private String AESDecryptionMethod(String string) throws UnsupportedEncodingException {`
> * `        byte[] EncryptedByte = string.getBytes("ISO-8859-1");`
> * `        String decryptedString = string;`
> * ``
> * `        byte[] decryption;`
> * ``
> * `        try {`
> * `            decipher.init(Cipher.DECRYPT_MODE, secretKeySpec);`
> * `            decryption = decipher.doFinal(EncryptedByte);`
> * `            decryptedString = new String(decryption);`
> * `        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {`
> * `            e.printStackTrace();`
> * `        }`
> * `        return decryptedString;`
> * `    }`
> * `}`
